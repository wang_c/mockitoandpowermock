package com.example.mock;

import com.example.mock.Enum.Person;
import com.example.mock.entity.User;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.powermock.api.mockito.PowerMockito.*;


//@SpringBootTest
@RunWith(PowerMockRunner.class)
@PrepareForTest({Person.class, User.class})
//@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
//@PowerMockIgnore({"javax.security.*"})
public class MockApplicationTests {
    @Mock
    User user;

    @Test
    public void mock() throws Exception {
        //mock类
        final String str1 = "normal method is mocked";
        when(user.returnName()).thenReturn(str1);
        assertThat(user.returnName()).isEqualTo(str1);
        //mock static方法
        final String str2 = "static method is mocked";
        mockStatic(User.class);
        when(User.returnNameStatic()).thenReturn(str2);
        assertThat(User.returnNameStatic()).isEqualTo(str2);
        //mock final方法
        final String str3 = "final method is mocked";
        User user3 = PowerMockito.mock(User.class);
        when(user3.returnNameFinal()).thenReturn(str3);
        assertThat(user3.returnNameFinal()).isEqualTo(str3);
        //mock private方法
        final String str4 = "private method be mocked";
        User user4 = spy(new User());
        when(user4, "returnNamePrivate").thenReturn(str4);
        assertThat(user4.callPrivateMethod()).isEqualTo(str4);
        //mock构造方法
        User user5 = new User(100);
        PowerMockito.whenNew(User.class).withArguments(1).thenReturn(user5);
        assertThat(new User(1)).isEqualTo(user5);
        //mock 枚举
        String personName = "enum is mocked";
        Person person = PowerMockito.mock(Person.class);
        Whitebox.setInternalState(Person.class, "E", person);
        when(person.E.getName()).thenReturn(personName);
        assertThat(person.E.getName()).isEqualTo(personName);
        assertThat(person.L.getName()).isEqualTo("leader");
    }
}
