package com.example.mock.entity;

import lombok.Data;
import org.springframework.stereotype.Repository;

@Data
@Repository
public class User {

    public int id;

    public String userName;

    public User() {

    }
    public User(int id) {
        this.id = id;
    }

    public String returnName() {
        return "ab";
    }

    public static String returnNameStatic() {
        return "static method";
    }

    public final String returnNameFinal() {
        return "final method";
    }

    public String callPrivateMethod() {return returnNamePrivate();}

    private String returnNamePrivate() {
        return "private method";
    }
}
