package com.example.mock.Enum;

/**
 * @author wangc
 */
public enum Person {
    /**
     * 公司人员枚举类
     */
    L("leader"),
    E("employee");
    private String name;
    Person(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
}
